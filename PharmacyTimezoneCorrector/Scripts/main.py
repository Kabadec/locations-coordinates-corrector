from columns import *
from print_timezone import *
from create_changes import *
import sqlite3

def count_changes_rows():
    cursor.execute(f"""SELECT count(*) as count
                    FROM changes""")
    return cursor.fetchone()[0]

def has_changes(ph_id, ph_loc_id, ph_sch_id):
    cursor.execute(f"""SELECT count(*) as count
                    FROM changes
                    WHERE "ph_id"={ph_id} and "ph_loc_id"={ph_loc_id} and "ph_sch_id"={ph_sch_id}""")
    return cursor.fetchone()[0] > 0




connection = sqlite3.connect("Data/timezones_corrections.db")
cursor = connection.cursor()
cursor.execute(f"""SELECT *
                    FROM timezones""")

all_timezones = cursor.fetchall()


print(f'Осталось заполнить таймзон: {len(all_timezones) - int(count_changes_rows())}')
print()


def update_timezone(timezone):
    print_timezone(timezone)
    create_changes(cursor, connection, timezone)
    pass


for timezone in all_timezones:
    if has_changes(timezone[ph_timezones_columns['ph_id']], timezone[ph_timezones_columns['ph_loc_id']], timezone[ph_timezones_columns['ph_sch_id']]) is True:
        continue
    update_timezone(timezone)


connection.close()