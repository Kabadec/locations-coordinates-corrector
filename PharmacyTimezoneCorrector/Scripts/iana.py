iana = [
    'Europe/Kaliningrad',  # MSK-01 - Kaliningrad	Canonical	+02:00	+02:00	EET	europe
    'Europe/Kirov',  # MSK+00 - Kirov	Canonical	+03:00	+03:00	MSK	europe
    'Europe/Moscow',  # MSK+00 - Moscow area	Canonical	+03:00	+03:00	MSK	europe
    'Europe/Simferopol',  # Crimea	Canonical	+03:00	+03:00	MSK	europe	Disputed - Reflects data in the TZDB.[note 2]
    'Europe/Volgograd',  # MSK+00 - Volgograd	Canonical	+03:00	+03:00	MSK	europe
    'Europe/Astrakhan',  # MSK+01 - Astrakhan	Canonical	+04:00	+04:00	+04	europe
    'Europe/Samara',  # MSK+01 - Samara, Udmurtia	Canonical	+04:00	+04:00	+04	europe
    'Europe/Saratov',  # MSK+01 - Saratov	Canonical	+04:00	+04:00	+04	europe
    'Europe/Ulyanovsk',  # MSK+01 - Ulyanovsk	Canonical	+04:00	+04:00	+04	europe
    'Asia/Yekaterinburg',  # MSK+02 - Urals	Canonical	+05:00	+05:00	+05	europe
    'Asia/Omsk',  # MSK+03 - Omsk	Canonical	+06:00	+06:00	+06	europe
    'Asia/Barnaul',  # MSK+04 - Altai	Canonical	+07:00	+07:00	+07	europe
    'Asia/Krasnoyarsk',  # MSK+04 - Krasnoyarsk area	Canonical	+07:00	+07:00	+07	europe
    'Asia/Novokuznetsk',  # MSK+04 - Kemerovo	Canonical	+07:00	+07:00	+07	europe
    'Asia/Novosibirsk',  # MSK+04 - Novosibirsk	Canonical	+07:00	+07:00	+07	europe
    'Asia/Tomsk',  # MSK+04 - Tomsk	Canonical	+07:00	+07:00	+07	europe
    'Asia/Irkutsk',  # MSK+05 - Irkutsk, Buryatia	Canonical	+08:00	+08:00	+08	europe
    'Asia/Chita',  # MSK+06 - Zabaykalsky	Canonical	+09:00	+09:00	+09	europe
    'Asia/Khandyga',  # MSK+06 - Tomponsky, Ust-Maysky	Canonical	+09:00	+09:00	+09	europe
    'Asia/Yakutsk',  # MSK+06 - Lena River	Canonical	+09:00	+09:00	+09	europe
    'Asia/Ust-Nera',  # MSK+07 - Oymyakonsky	Canonical	+10:00	+10:00	+10	europe
    'Asia/Vladivostok',  # MSK+07 - Amur River	Canonical	+10:00	+10:00	+10	europe
    'Asia/Magadan',  # MSK+08 - Magadan	Canonical	+11:00	+11:00	+11	europe
    'Asia/Sakhalin',  # MSK+08 - Sakhalin Island	Canonical	+11:00	+11:00	+11	europe
    'Asia/Srednekolymsk',  # MSK+08 - Sakha (E), N Kuril Is	Canonical	+11:00	+11:00	+11	europe
    'Asia/Anadyr',  # MSK+09 - Bering Sea	Canonical	+12:00	+12:00	+12	europe
    'Asia/Kamchatka',  # MSK+09 - Kamchatka	Canonical	+12:00	+12:00	+12	europe	
]