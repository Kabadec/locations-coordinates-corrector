import sqlite3
from columns import *

changes_columns = {
    'id': 0,
    'city_id': 1,
    'city_name': 2,
    'find_city_name': 3,
    'longitude': 4,
    'latitude': 5
}
file_path = 'Data/update_timezones_sql_queries.txt'
schedule_table_name = 'partners_pharmacy_schedule'
settlement_table_name = 'locations_settlement'


connection = sqlite3.connect("Data/timezones_corrections.db")
cursor = connection.cursor()
stream = open(file_path, "w")

cursor.execute(f"""SELECT * 
                    FROM changes 
                    WHERE is_update_ph_loc is true""")
schedule_changes = cursor.fetchall()

for schedule_change in schedule_changes:
    sql_query = f'''UPDATE {schedule_table_name} SET "UpdatedAt" = current_timestamp, "Timezone"='{schedule_change[ph_changes_columns["ph_loc_new_value"]]}' WHERE "Id"={schedule_change[ph_changes_columns["ph_sch_id"]]};'''
    print(sql_query)
    stream.writelines(sql_query + '\n')
    pass

print()
stream.writelines('\n')

cursor.execute(f"""SELECT * 
                    FROM changes 
                    WHERE is_update_settlement is true""")
settlements_changes = cursor.fetchall()

settlements_changes_dict = {}
for settlement_change in settlements_changes:
    settlement_id = settlement_change[ph_changes_columns["settlement_id"]]
    timezone = settlement_change[ph_changes_columns["settlement_new_value"]]
    if settlement_id in settlements_changes_dict.keys() and settlements_changes_dict[settlement_id] != timezone:
        raise ValueError('Wrong source.')
    settlements_changes_dict[settlement_id] = timezone
    pass

for id, timezone in settlements_changes_dict.items():
    sql_query = f'''UPDATE {settlement_table_name} SET "UpdatedAt" = current_timestamp, "Timezone"='{timezone}' WHERE "Id"={id};'''
    print(sql_query)
    stream.writelines(sql_query + '\n')
    pass

stream.close()
connection.close()
