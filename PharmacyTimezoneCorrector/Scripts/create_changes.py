from time import sleep
from columns import *
from select_timezone import *


def add_changes_in_db(
        cursor,
        connection,
        ph_id,
        ph_loc_id,
        ph_sch_id,
        settlement_id,
        is_update_ph_loc,
        ph_loc_new_value,
        is_update_settlement,
        settlement_new_value):
    cursor.execute(f"""INSERT INTO changes (ph_id, ph_loc_id, ph_sch_id, settlement_id, is_update_ph_loc, ph_loc_new_value, is_update_settlement, settlement_new_value)
                       VALUES( {ph_id}, {ph_loc_id}, {ph_sch_id}, {settlement_id}, {is_update_ph_loc}, '{ph_loc_new_value}', {is_update_settlement}, '{settlement_new_value}');""")
    connection.commit()
    sleep(0.1)
    pass


def change_settlement_timezone_on_schedule_timezone(cursor, connection, timezone):
    add_changes_in_db(
        cursor,
        connection,
        timezone[ph_timezones_columns['ph_id']],
        timezone[ph_timezones_columns['ph_loc_id']],
        timezone[ph_timezones_columns['ph_sch_id']],
        timezone[ph_timezones_columns['settlement_id']],
        False,
        '',
        True,
        timezone[ph_timezones_columns['ph_sch_timezone']]
    )
    pass


def change_schedule_timezone_on_settlement_timezone(cursor, connection, timezone):
    add_changes_in_db(
        cursor,
        connection,
        timezone[ph_timezones_columns['ph_id']],
        timezone[ph_timezones_columns['ph_loc_id']],
        timezone[ph_timezones_columns['ph_sch_id']],
        timezone[ph_timezones_columns['settlement_id']],
        True,
        timezone[ph_timezones_columns['settlement_timezone']],
        False,
        ''
    )
    pass


def set_custom_timezone_on_schedule(cursor, connection, timezone):
    selected_timezone = select_timezone()
    add_changes_in_db(
        cursor,
        connection,
        timezone[ph_timezones_columns['ph_id']],
        timezone[ph_timezones_columns['ph_loc_id']],
        timezone[ph_timezones_columns['ph_sch_id']],
        timezone[ph_timezones_columns['settlement_id']],
        True,
        selected_timezone,
        False,
        ''
    )
    pass


def set_custom_timezone_on_settlement(cursor, connection, timezone):
    selected_timezone = select_timezone()
    add_changes_in_db(
        cursor,
        connection,
        timezone[ph_timezones_columns['ph_id']],
        timezone[ph_timezones_columns['ph_loc_id']],
        timezone[ph_timezones_columns['ph_sch_id']],
        timezone[ph_timezones_columns['settlement_id']],
        False,
        '',
        True,
        selected_timezone
    )
    pass

def set_custom_timezone_on_schedule_and_settlement(cursor, connection, timezone):
    selected_timezone = select_timezone()
    add_changes_in_db(
        cursor,
        connection,
        timezone[ph_timezones_columns['ph_id']],
        timezone[ph_timezones_columns['ph_loc_id']],
        timezone[ph_timezones_columns['ph_sch_id']],
        timezone[ph_timezones_columns['settlement_id']],
        True,
        selected_timezone,
        True,
        selected_timezone
    )
    pass

def skip_timezone(cursor, connection, timezone):
    add_changes_in_db(
        cursor,
        connection,
        timezone[ph_timezones_columns['ph_id']],
        timezone[ph_timezones_columns['ph_loc_id']],
        timezone[ph_timezones_columns['ph_sch_id']],
        timezone[ph_timezones_columns['settlement_id']],
        False,
        '',
        False,
        ''
    )
    pass

def create_changes(cursor, connection, timezone):
    print('1. Choose: Schedule timezone')
    print('2. Choose: Settlement timezone')
    print('3. Set custom timezone on Schedule and Settlement')
    print('4. Set custom timezone on Schedule')
    print('5. Set custom timezone on Settlement')
    print('6. Skip')
    digit = int(input())
    if digit == 1:
        change_settlement_timezone_on_schedule_timezone(cursor, connection, timezone)
        return
    elif digit == 2:
        change_schedule_timezone_on_settlement_timezone(cursor, connection, timezone)
        return
    elif digit == 3:
        set_custom_timezone_on_schedule_and_settlement(cursor, connection, timezone)
        return
    elif digit == 4:
        set_custom_timezone_on_schedule(cursor, connection, timezone)
        return
    elif digit == 5:
        set_custom_timezone_on_settlement(cursor, connection, timezone)
        return
    elif digit == 6:
        skip_timezone(cursor, connection, timezone)
        return
    raise ValueError('Wrong input.')
    pass