import csv
import json

city_columns = [
    "Id",
    "Deleted",
    "CreatedAt",
    "UpdatedAt",
    "AgglomerationId",
    "PublicId",
    "Slug",
    "NameI",
    "NameR",
    "NameD",
    "NameV",
    "NameT",
    "NameP",
    "IsCoreCity",
    "Longitude",
    "Latitude",
    "Timezone",
    "IsActive"
]

agglomeration_columns = [
    'Id',
    'Deleted',
    'CreatedAt',
    'UpdatedAt',
    'PublicId',
    'IsDefault',
    'DistanceInKm',
    'NeedExportToProduction',
    'NeedExportToSandbox'
]

ph_timezones_columns = [
    'ph_id',
    'ph_display_name',
    'ph_loc_id',
    'ph_loc_city',
    'ph_loc_region',
    'ph_loc_settlement',
    'ph_loc_raw_city',
    'ph_loc_raw_region',
    'ph_loc_raw_settlement',
    'ph_loc_longitude',
    'ph_loc_latitude',
    'ph_sch_id',
    'ph_sch_timezone',
    'ph_sch_raw_timezone',
    'ph_sch_raw_timezone_gmt',
    'settlement_id',
    'settlement_namei',
    'settlement_timezone',
    'settlement_longitude',
    'settlement_latitude',
]

ph_changes_columns = [
    'id',
    'ph_id',
    'ph_loc_id',
    'ph_sch_id',
    'settlement_id',
    'is_update_ph_loc',
    'ph_loc_new_value',
    'is_update_settlement',
    'settlement_new_value',
]



table_name = 'timezones'
file_name = 'Data/pgweb-1721645649.csv'
out_file_name = 'Data/insert_pharmacies_timezones.txt'
allowed_columns = ph_timezones_columns



stream = open(out_file_name, "w")

with open(file_name) as f:
    csv_reader = csv.DictReader(f)
    csv_data = list(csv_reader)
    csv_column_names = list(dict(csv_data[0]).keys())

filtered_columns = []
for column_name in allowed_columns:
    if column_name in csv_column_names:
        filtered_columns.append(column_name)


stream.writelines(f"insert into {table_name}\n")
filtered_columns_names = f"({str(json.dumps(filtered_columns)).lstrip('[').rstrip(']')})"
stream.writelines(filtered_columns_names + "\n")
stream.writelines(f"values\n")


values = []

for row in csv_data:
    row_values = []
    for column in filtered_columns:
        row_values.append(row[column])
    values.append(f"({str(row_values).lstrip('[').rstrip(']')})")


str_values = ",\n".join(values)
stream.write(str_values)
stream.write(";")
stream.close()
