import sqlite3
from columns import *

changes_columns = {
    'id': 0,
    'city_id': 1,
    'city_name': 2,
    'find_city_name': 3,
    'longitude': 4,
    'latitude': 5
}
file_path = 'Data/update_timezones_sql_queries.txt'
schedule_table_name = 'partners_pharmacy_schedule'
settlement_table_name = 'locations_settlement'


connection = sqlite3.connect("Data/timezones_corrections.db")
cursor = connection.cursor()
cursor.execute(f"""SELECT * 
                    FROM changes 
                    WHERE is_update_ph_loc is false and is_update_settlement is false""")
wrong_pharmacies = cursor.fetchall()

wrong_ids = []

for pharmacy in wrong_pharmacies:
    wrong_ids.append(pharmacy[ph_timezones_columns['ph_id']])

print(wrong_ids)
connection.close()
