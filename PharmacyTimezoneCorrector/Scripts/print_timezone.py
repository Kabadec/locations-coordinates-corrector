from prettytable import PrettyTable
from columns import *

def print_timezone(timezone):
    print('Pharmacy:')
    table = PrettyTable(['id', 'display_name'])
    table.add_row([
        timezone[ph_timezones_columns['ph_id']],
        timezone[ph_timezones_columns['ph_display_name']]
    ])
    print(table)
    print()

    print(f'2gis координаты локации аптеки: {create_browser_2gis_geo_url(timezone[ph_timezones_columns["ph_loc_longitude"]], timezone[ph_timezones_columns["ph_loc_latitude"]])}')
    print('Pharmacy Location:')
    table = PrettyTable(['id', 'city', 'region', 'settlement', 'raw_city', 'raw_region', 'raw_settlement', 'longitude', 'latitude'])
    table.add_row([
        timezone[ph_timezones_columns['ph_loc_id']],
        timezone[ph_timezones_columns['ph_loc_city']],
        timezone[ph_timezones_columns['ph_loc_region']],
        timezone[ph_timezones_columns['ph_loc_settlement']],
        timezone[ph_timezones_columns['ph_loc_raw_city']],
        timezone[ph_timezones_columns['ph_loc_raw_region']],
        timezone[ph_timezones_columns['ph_loc_raw_settlement']],
        timezone[ph_timezones_columns['ph_loc_longitude']],
        timezone[ph_timezones_columns['ph_loc_latitude']]
    ])
    print(table)
    print()

    print('Pharmacy Schedule:')
    table = PrettyTable(['id', 'timezone', 'raw_timezone', 'raw_timezone_gmt'])
    table.add_row([
        timezone[ph_timezones_columns['ph_sch_id']],
        timezone[ph_timezones_columns['ph_sch_timezone']],
        timezone[ph_timezones_columns['ph_sch_raw_timezone']],
        timezone[ph_timezones_columns['ph_sch_raw_timezone_gmt']],
    ])
    print(table)
    print()

    print('Settlement:')
    table = PrettyTable(['id', 'name_i', 'timezone', 'longitude', 'latitude'])
    table.add_row([
        timezone[ph_timezones_columns['settlement_id']],
        timezone[ph_timezones_columns['settlement_namei']],
        timezone[ph_timezones_columns['settlement_timezone']],
        timezone[ph_timezones_columns['settlement_longitude']],
        timezone[ph_timezones_columns['settlement_latitude']],
    ])
    print(table)
    print(f'2gis координаты населенного пункта: {create_browser_2gis_geo_url(timezone[ph_timezones_columns["settlement_longitude"]], timezone[ph_timezones_columns["settlement_latitude"]])}')
    print()
    pass

def create_browser_2gis_geo_url(lon, lat):
    gis_url = 'https://2gis.ru/geo/{longitude}%2C{latitude}'
    return gis_url.format(longitude=lon, latitude=lat)