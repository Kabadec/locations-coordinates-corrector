ph_timezones_columns = {
    'id': 0,
    'ph_id': 1,
    'ph_display_name': 2,
    'ph_loc_id': 3,
    'ph_loc_city': 4,
    'ph_loc_region': 5,
    'ph_loc_settlement': 6,
    'ph_loc_raw_city': 7,
    'ph_loc_raw_region': 8,
    'ph_loc_raw_settlement': 9,
    'ph_loc_longitude': 10,
    'ph_loc_latitude': 11,
    'ph_sch_id': 12,
    'ph_sch_timezone': 13,
    'ph_sch_raw_timezone': 14,
    'ph_sch_raw_timezone_gmt': 15,
    'settlement_id': 16,
    'settlement_namei': 17,
    'settlement_timezone': 18,
    'settlement_longitude': 19,
    'settlement_latitude': 20,
}

ph_changes_columns = {
    'id': 0,
    'ph_id': 1,
    'ph_loc_id': 2,
    'ph_sch_id': 3,
    'settlement_id': 4,
    'is_update_ph_loc': 5,
    'ph_loc_new_value': 6,
    'is_update_settlement': 7,
    'settlement_new_value': 8,
}