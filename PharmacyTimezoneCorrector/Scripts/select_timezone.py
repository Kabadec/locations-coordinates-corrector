from iana import *


def show_timezones():
    print('Доступные iana таймзоны:')
    for i, timezone in enumerate(iana):
        print(f'{i + 1}. {timezone}.')
    print()
    pass


def select_timezone():
    show_timezones()
    print('Введите номер нужной таймзоны:')
    index = int(input()) - 1
    if index < 0 or index >= len(iana):
        raise ValueError('Wrong input.')
    return iana[index]
