import csv
import json

city_columns = [
    "Id",
    "Deleted",
    "CreatedAt",
    "UpdatedAt",
    "AgglomerationId",
    "PublicId",
    "Slug",
    "NameI",
    "NameR",
    "NameD",
    "NameV",
    "NameT",
    "NameP",
    "IsCoreCity",
    "Longitude",
    "Latitude",
    "Timezone",
    "IsActive"
]

agglomeration_columns = [
    'Id',
    'Deleted',
    'CreatedAt',
    'UpdatedAt',
    'PublicId',
    'IsDefault',
    'DistanceInKm',
    'NeedExportToProduction',
    'NeedExportToSandbox'
]



table_name = 'locations_agglomeration_city'
file_name = 'Data/prod_locations_agglomeration_city.csv'
out_file_name = 'Data/insert_cities.txt'
allowed_columns = city_columns



stream = open(out_file_name, "w")

with open(file_name) as f:
    csv_reader = csv.DictReader(f)
    csv_data = list(csv_reader)
    csv_column_names = list(dict(csv_data[0]).keys())

filtered_columns = []
for column_name in allowed_columns:
    if column_name in csv_column_names:
        filtered_columns.append(column_name)


stream.writelines(f"insert into {table_name}\n")
filtered_columns_names = f"({str(json.dumps(filtered_columns)).lstrip('[').rstrip(']')})"
stream.writelines(filtered_columns_names + "\n")
stream.writelines(f"values\n")


values = []

for row in csv_data:
    row_values = []
    for column in filtered_columns:
        row_values.append(row[column])
    values.append(f"({str(row_values).lstrip('[').rstrip(']')})")


str_values = ",\n".join(values)
stream.write(str_values)
stream.write(";")
stream.close()
