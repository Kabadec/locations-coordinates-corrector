from dadata import Dadata
from Locating.api_2gis import *
from city_columns import *
from prettytable import PrettyTable
from Tools.utc_offset import *

kochergin38m_token = '2349ed3bd4bd35b3f91fc0dfe4de520ec6e698f8'
kochergin38m_secret = '7c4a435cb5a7f0eb97b84ec1a41f8944cb70d0c3'

blaaalb_token = '01da6c9f4587d90dbbc7b0255faf3f761f1b806b'
blaaalb_secret = '8f45758a1af418975dc1f6d4156035132fc0c0ca'

kochergin_m_token = 'f8737755e29136688fe8d99db08eb0bdc658ab1d'
kochergin_m_secret = '35aa92b29df989d9bb0a3acd8ce64485851facfb'

mikhail_kochergin_98_token = 'd1eb07db4f17a3e2001b2b28d93c1191c06f52fd'
mikhail_kochergin_98_secret = 'a2294daaa84d24d208de46021886208a9bf61a22'

def request_search_dadata(q):
    token = kochergin38m_token
    secret = kochergin38m_secret
    dadata = Dadata(token, secret)
    result = dadata.clean("address", q)
    return result


def print_compare_dadata_response_with_location(response, location):
    location_name = location[city_columns['NameI']]
    if response['result'] is None:
        print(f'Dadata не нашёл локации по запросу: {str(location_name)}, Id: {location[city_columns["Id"]]}')
        return
    print(f'Location: {location}')
    print(f'Response: {response}')
    print('Найденная локация:')
    table = PrettyTable(['Field', 'Query', 'Find'])
    table.align = "c"
    table.add_row(['Name', location_name, response["result"]])
    table.add_row(['Timezone', response['timezone'][4:], get_offset_from_timezone(location[city_columns['Timezone']])])
    table.add_row(['Region with type', '-', response["region_with_type"]])
    table.add_row(['Settlement type', '-', response["settlement_type_full"]])
    print(table)
    pass

# Response example:
# {
#     "source": "Новосибирск",
#     "result": "г Новосибирск",
#     "postal_code": "630000",
#     "country": "Россия",
#     "country_iso_code": "RU",
#     "federal_district": "Сибирский",
#     "region_fias_id": "1ac46b49-3209-4814-b7bf-a509ea1aecd9",
#     "region_kladr_id": "5400000000000",
#     "region_iso_code": "RU-NVS",
#     "region_with_type": "Новосибирская обл",
#     "region_type": "обл",
#     "region_type_full": "область",
#     "region": "Новосибирская",
#     "area_fias_id": null,
#     "area_kladr_id": null,
#     "area_with_type": null,
#     "area_type": null,
#     "area_type_full": null,
#     "area": null,
#     "city_fias_id": "8dea00e3-9aab-4d8e-887c-ef2aaa546456",
#     "city_kladr_id": "5400000100000",
#     "city_with_type": "г Новосибирск",
#     "city_type": "г",
#     "city_type_full": "город",
#     "city": "Новосибирск",
#     "city_area": null,
#     "city_district_fias_id": null,
#     "city_district_kladr_id": null,
#     "city_district_with_type": null,
#     "city_district_type": null,
#     "city_district_type_full": null,
#     "city_district": null,
#     "settlement_fias_id": null,
#     "settlement_kladr_id": null,
#     "settlement_with_type": null,
#     "settlement_type": null,
#     "settlement_type_full": null,
#     "settlement": null,
#     "street_fias_id": null,
#     "street_kladr_id": null,
#     "street_with_type": null,
#     "street_type": null,
#     "street_type_full": null,
#     "street": null,
#     "stead_fias_id": null,
#     "stead_kladr_id": null,
#     "stead_cadnum": null,
#     "stead_type": null,
#     "stead_type_full": null,
#     "stead": null,
#     "house_fias_id": null,
#     "house_kladr_id": null,
#     "house_cadnum": null,
#     "house_type": null,
#     "house_type_full": null,
#     "house": null,
#     "block_type": null,
#     "block_type_full": null,
#     "block": null,
#     "entrance": null,
#     "floor": null,
#     "flat_fias_id": null,
#     "flat_cadnum": null,
#     "flat_type": null,
#     "flat_type_full": null,
#     "flat": null,
#     "flat_area": null,
#     "square_meter_price": null,
#     "flat_price": null,
#     "postal_box": null,
#     "fias_id": "8dea00e3-9aab-4d8e-887c-ef2aaa546456",
#     "fias_code": "54000001000000000000000",
#     "fias_level": "4",
#     "fias_actuality_state": "0",
#     "kladr_id": "5400000100000",
#     "capital_marker": "2",
#     "okato": "50401000000",
#     "oktmo": "50701000001",
#     "tax_office": "5400",
#     "tax_office_legal": "5400",
#     "timezone": "UTC+7",
#     "geo_lat": "55.028191",
#     "geo_lon": "82.9211489",
#     "beltway_hit": null,
#     "beltway_distance": null,
#     "qc_geo": 4,
#     "qc_complete": 3,
#     "qc_house": 10,
#     "qc": 0,
#     "unparsed_parts": null,
#     "metro": null
# }
