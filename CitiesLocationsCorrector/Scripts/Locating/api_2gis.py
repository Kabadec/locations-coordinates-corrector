import json
import requests
import urllib.parse

from prettytable import PrettyTable
from Tools.utc_offset import *
from city_columns import *
from bcolors import *

api_key = '7f2a3461-20c9-4ff8-b6e5-83d45b9de46f'


def print_pharmacies_around_location(response):
    if response['meta']['code'] == 404 or 'result' not in response:
        print('Найдено аптек поблизости: 0.')
        return

    find_names = []
    for item in response['result']['items']:
        find_names.append(item['name'])
    print(f'Найдено аптек поблизости: {len(find_names)}. {" || ".join(find_names)}')
    pass


def print_browser_2gis_links(location_name, lon, lat):
    print(f'2gis поиск: {create_browser_2gis_search_url(location_name)}')
    print(f'2gis координаты: {create_browser_2gis_geo_url(lon, lat)}')
    print(f'2gis аптеки по координатам: {create_browser_2gis_geo_url_with_pharmacy_search(lon, lat)}')
    pass


def print_compare_2gis_response_with_location(response, location):
    location_name = location[city_columns['NameI']]
    if response['meta']['code'] == 404:
        print('2gis не нашёл локации по запросу: ' + str(location_name))
        return
    find_locations = response['result']['items']
    print(f'Location: {location}')
    print(f'Response: {find_locations}')
    print(f'Найдено локаций: {len(find_locations)}')
    counter = 1
    table = PrettyTable(['Index', 'Name', 'Timezone equal', 'Find Location offset', 'Location offset'])
    table.align = "c"
    location_timezone_offset = get_offset_from_timezone(location[city_columns['Timezone']])
    for find_location in find_locations:
        find_location_geo = find_location['point']
        find_location_timezone_str = get_timezone_str_from_coordinates(find_location_geo["lon"], find_location_geo["lat"])
        find_location_timezone_offset = get_offset_from_timezone(find_location_timezone_str)
        timezone_equal = bcolors_true_text if find_location_timezone_offset == location_timezone_offset else bcolors_false_text
        find_location_full_name = find_location['full_name']
        table.add_row([counter, find_location_full_name, timezone_equal, find_location_timezone_offset, location_timezone_offset])
        counter = counter + 1
    print(table)
    pass


def create_browser_2gis_search_url(q):
    gis_url = '2gis.ru/search/{q}'
    formatted_gis_url = gis_url.format(q=q)
    return 'https://' + urllib.parse.quote(formatted_gis_url)


def create_browser_2gis_geo_url(lon, lat):
    gis_url = 'https://2gis.ru/geo/{longitude}%2C{latitude}'
    return gis_url.format(longitude=lon, latitude=lat)


def create_browser_2gis_geo_url_with_pharmacy_search(lon, lat):
    gis_url = 'https://2gis.ru/search/%D0%B0%D0%BF%D1%82%D0%B5%D0%BA%D0%B0?m={longitude}%2C{latitude}%2F15.34'
    return gis_url.format(longitude=lon, latitude=lat)


def create_api_search_url(q):
    api_2gis_url = 'https://catalog.api.2gis.com/3.0/items/geocode?q={0}&fields=items.point,items.geometry.centroid&key={1}'
    return api_2gis_url.format(q, api_key)


def create_api_search_pharmacies_url(lon, lat):
    api_2gis_url = 'https://catalog.api.2gis.com/3.0/items?q=%D0%B0%D0%BF%D1%82%D0%B5%D0%BA%D0%B0&type=branch&point={0},{1}&radius=1500&key={2}'
    return api_2gis_url.format(lon, lat, api_key)


def request_search_pharmacies_2gis(lon, lat):
    url = create_api_search_pharmacies_url(lon, lat)
    response = requests.get(url)
    return response.json()

def request_search_2gis(q):
    url = create_api_search_url(q)
    response = requests.get(url)
    return response.json()

# print(json.dumps(result, indent=4, ensure_ascii=False))
# point = result['result']['items'][0]['point']
# print(point)
# print(gis_url.format(longitude=point['lon'], latitude=point['lat']))


# Response example:
# {
#     "meta": {
#         "api_version": "3.0.17799",
#         "code": 200,
#         "issue_date": "20240516"
#     },
#     "result": {
#         "items": [
#             {
#                 "full_name": "Новосибирск",
#                 "geometry": {
#                     "centroid": "POINT(82.942781 55.017395)"
#                 },
#                 "id": "141360258613345",
#                 "name": "Новосибирск",
#                 "point": {
#                     "lat": 55.017395,
#                     "lon": 82.942781
#                 },
#                 "subtype": "city",
#                 "type": "adm_div"
#             },
#             {
#                 "address_name": "13",
#                 "building_name": "Галерея Новосибирск, торгово-развлекательный центр",
#                 "full_name": "Новосибирск, Галерея Новосибирск, торгово-развлекательный центр",
#                 "geometry": {
#                     "centroid": "POINT(82.92223 55.043715)"
#                 },
#                 "id": "141373143680762",
#                 "name": "Галерея Новосибирск, торгово-развлекательный центр",
#                 "point": {
#                     "lat": 55.043715,
#                     "lon": 82.92223
#                 },
#                 "purpose_name": "Shopping and entertainment complex",
#                 "type": "building"
#             }
#         ],
#         "total": 2
#     }
# }
