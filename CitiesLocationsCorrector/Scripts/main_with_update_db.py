from time import sleep
import psycopg2
from psycopg2.extras import NamedTupleCursor
from api_dadata import request_search_dadata, print_dadata_response
from api_2gis import request_search_2gis, create_2gis_search_url, print_2gis_response
from datetime import datetime, timezone


def update_location_in_db(conn, curs, table_name, row_id, lon, lat):
    dt = datetime.now(timezone.utc)
    update_sql = f"""UPDATE {table_name}
                     SET "UpdatedAt"=%s, "Longitude"=%s, "Latitude"=%s
                     WHERE "Id"=%s;"""
    curs.execute(update_sql, (dt, lon, lat, row_id))
    conn.commit()
    pass


def update_location_with_2gis(conn, curs, table_name, location):
    location_name = location.NameI
    response = request_search_2gis(location_name)
    print_2gis_response(response, location_name)
    if response['meta']['code'] == 404:
        return
    target_location_geo = response['result']['items'][0]['point']
    print()
    print('Обновляем данные в базе данных (y/n)?')
    answer = input()
    if answer == 'y' or answer == 'Y':
        print(f'Обновляем локацию {location_name}...')
        update_location_in_db(conn, curs, table_name, location.Id, target_location_geo["lon"], target_location_geo["lat"])
        print(f'Успешно обновили локацию {location_name}.')
    else:
        print(f'Не обновили локацию: {location_name}')
    pass


def update_location_with_dadata(conn, curs, table_name, location):
    location_name = location.NameI
    response = request_search_dadata(location_name)
    print_dadata_response(response, location_name)
    if response['result'] is None:
        return
    lon = response["geo_lon"]
    lat = response["geo_lat"]
    print()
    print('Обновляем данные в базе данных (y/n)?')
    answer = input()
    if answer == 'y' or answer == 'Y':
        print(f'Обновляем локацию {location_name}...')
        update_location_in_db(conn, curs, table_name, location.Id, lon, lat)
        print(f'Успешно обновили локацию {location_name}.')
    else:
        print(f'Не обновили локацию: {location_name}')
    pass


def update_location(conn, curs, table_name, location):
    update_with_2gis = False
    update_with_dadata = True
    print()
    print()
    print(location)
    location_name = location.NameI
    print(f'Ищем локацию по запросу: {location_name}, Id в бд: {location.Id}')
    print(f'2gis поиск: {create_2gis_search_url(location_name)}')
    if update_with_2gis:
        print('Используем 2gis')
        update_location_with_2gis(conn, curs, table_name, location)
    if update_with_dadata:
        print('Используем dadata')
        update_location_with_dadata(conn, curs, table_name, location)
    pass


def update_locations(conn, curs, table_name):
    curs.execute(f"""SELECT x.* 
                    FROM {table_name} x 
                    WHERE "Longitude" = 0 or "Latitude" = 0
                    ORDER BY "Id" ASC""")
    invalid_locations = curs.fetchall()
    print('Найдено локаций с невалидными координатами:' + str(len(invalid_locations)))
    for location in invalid_locations:
        update_location(conn, curs, table_name, location)
        sleep(0.5)


conn = psycopg2.connect(dbname='catalog_dev', user='phrm', password='write_pass_here', host='178.170.242.194', port='10001')
with conn.cursor(cursor_factory=NamedTupleCursor) as cursor:
    update_locations(conn, cursor, 'public.locations_agglomeration_city')
conn.close()
