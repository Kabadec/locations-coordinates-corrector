import csv
import sqlite3


table_name = 'locations_agglomeration_city_test'
file_name = 'Data/locations_agglomeration_city_new.csv'
db_name = 'Data/cities_locations_changes.db'

connection = sqlite3.connect(db_name)
cursor = connection.cursor()
with open(file_name) as f:
    reader = csv.reader(f)
    data = list(reader)
    
cursor.execute(f"""create table {table_name}
(
    "Id"              bigint                not null,
    "Deleted"         boolean               not null,
    "CreatedAt"       timestamp             not null,
    "UpdatedAt"       timestamp             not null,
    "AgglomerationId" bigint                not null,
    "PublicId"        uuid                  not null,
    "Slug"            text,
    "NameI"           text,
    "NameR"           text,
    "NameD"           text,
    "NameV"           text,
    "NameT"           text,
    "NameP"           text,
    "IsCoreCity"      boolean               not null,
    "IsSatelliteCity" boolean               not null,
    "Longitude"       double precision      not null,
    "Latitude"        double precision      not null,
    "Timezone"        text,
    "IsActive"        boolean default false not null,
    constraint "PK_{table_name}"
        primary key ("Id"),
    constraint "AK_{table_name}_PublicId"
        unique ("PublicId")
);""")
cursor.execute(f"""create index "IX_{table_name}_AgglomerationId"
    on {table_name} ("AgglomerationId");""")
cursor.execute(f"""create unique index "IX_{table_name}_Slug"
    on {table_name} ("Slug");""")

for row in data:
    cursor.execute(f"INSERT INTO {table_name} (Id, Deleted, CreatedAt, UpdatedAt, AgglomerationId, PublicId, Slug, NameI, NameR, NameD, NameV, NameT, NameP, IsCoreCity, IsSatelliteCity, Longitude, Latitude, Timezone, IsActive) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", row)

connection.commit()
connection.close()

# create table locations_agglomeration_city
# (
#     "Id"              bigint                not null,
#     "Deleted"         boolean               not null,
#     "CreatedAt"       timestamp             not null,
#     "UpdatedAt"       timestamp             not null,
#     "AgglomerationId" bigint                not null,
#     "PublicId"        uuid                  not null,
#     "Slug"            text,
#     "NameI"           text,
#     "NameR"           text,
#     "NameD"           text,
#     "NameV"           text,
#     "NameT"           text,
#     "NameP"           text,
#     "IsCoreCity"      boolean               not null,
#     "IsSatelliteCity" boolean               not null,
#     "Longitude"       double precision      not null,
#     "Latitude"        double precision      not null,
#     "Timezone"        text,
#     "IsActive"        boolean default false not null,
#     constraint "PK_locations_agglomeration_city"
#         primary key ("Id"),
#     constraint "AK_locations_agglomeration_city_PublicId"
#         unique ("PublicId")
# );
# create index "IX_locations_agglomeration_city_AgglomerationId"
#     on locations_agglomeration_city ("AgglomerationId");
# 
# create unique index "IX_locations_agglomeration_city_Slug"
#     on locations_agglomeration_city ("Slug");
