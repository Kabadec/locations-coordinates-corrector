import datetime
import math
import pytz
from timezonefinder import TimezoneFinder


def get_offset_from_timezone(timezone):
    location_timezone = pytz.timezone(timezone)
    pacific_now = datetime.datetime.now(location_timezone)
    offset = pacific_now.utcoffset().total_seconds()/60/60
    return math.ceil(offset)


def get_timezone_str_from_coordinates(lon, lat):
    tf = TimezoneFinder()
    return tf.timezone_at(lng=lon, lat=lat)
