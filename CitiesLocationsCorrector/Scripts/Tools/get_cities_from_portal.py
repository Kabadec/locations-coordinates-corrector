import requests as requests
import math
from prettytable import PrettyTable

url = 'https://expero.ru/api/v2/location/cities'
take_per_request = 100

response = requests.get(url + '?need_elements=false')
cities_count = response.json()['pagination']['total_count']
print('Total cities count: ' + str(cities_count))

num_requests = math.ceil(cities_count / 100)
print(num_requests)

table = PrettyTable(['slug', 'longitude', 'latitude'])
table.align = "r"
table.get_csv_string()


def add_cities_to_table(t, take, skip):
    new_url = url + '?take=' + str(take) + '&skip=' + str(skip)
    els = requests.get(new_url).json()['elements']
    counter = 0
    for el in els:
        t.add_row([el['slug'], el["longitude"], el["latitude"]])
        if el["longitude"] == 0.0 or el["latitude"] == 0.0:
            counter += 1
    print(counter)
    pass


for i in range(num_requests):
    skip = i * take_per_request
    add_cities_to_table(table, take_per_request, skip)

print(table)
print("Num cities: " + str(len(table.rows)))
