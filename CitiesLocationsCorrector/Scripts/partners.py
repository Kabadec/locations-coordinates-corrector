from numpy.core.defchararray import lower

partners_names = [
    'Легко',
    'Гармония',
    'Добрая аптека',
    'фармация',
    'Айти аптека',
    'Ригла',
    'Ваш Доктор',
    'Фармленд',
    'Алоэ',
    'Планета Здоровья',
    'Фармсервис',
    'Диалог',
    'Невис',
    '36.6',
    '36,6',
    'Моя аптека',
    'ВИТА',
    'Неболейка',
    'Прайд',
    'Фармакопейка',
    'Живика',
    'Фармдисконт',
    'Дешёвая',
    'Дешевая',
    'Лекаптека',
    'Озерки',
    'Городская аптека',
    'Аптечный пункт',
    'ГОРЗДРАВ',
    'ЛекОптТорг',
    'ЛенОблФарм',
    'Шах',
    'Мфс',
    'Госаптека',
    'ДА, аптека',
    'Будь здоров!',
    'Алия',
    'Доктор Столетов'
]

def is_partner_pharmacy(pharmacy_name):
    lower_pharmacy_name = str(lower(pharmacy_name))
    for partner_name in partners_names:
        lower_partner_name = str(lower(partner_name))
        if lower_partner_name in lower_pharmacy_name:
            return True
    return False


# test_name = 'Фармакопейка, аптечный пункт'
# print(is_partner_pharmacy(test_name))
