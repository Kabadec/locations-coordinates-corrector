import sqlite3


changes_columns = {
    'id': 0,
    'city_id': 1,
    'city_name': 2,
    'find_city_name': 3,
    'longitude': 4,
    'latitude': 5
}
file_path = 'Data/update_cities_sql_queries.txt'
table_name = 'locations_agglomeration_city'


def get_file_path(index):
    file_path_name = 'Data/update_cities_sql_queries'
    extension = '.txt'
    if index is None:
        return file_path_name + extension
    return file_path_name + '_' + str(index) + extension

connection = sqlite3.connect("Data/cities_locations_changes.db")
cursor = connection.cursor()

cursor.execute(f"""SELECT * 
                    FROM changes 
                    WHERE "longitude" != -1 and "latitude" != -1
                    ORDER BY "Id" ASC""")
cities_changes = cursor.fetchall()

index = 0
step = 1000
while True:
    stream = open(get_file_path(index), "w")
    for city_change in cities_changes[(index * step):((index + 1) * step)]:
        sql_query = f'UPDATE {table_name} SET "UpdatedAt" = current_timestamp, "Longitude"={city_change[changes_columns["longitude"]]}, "Latitude"={city_change[changes_columns["latitude"]]} WHERE "Id"={city_change[changes_columns["city_id"]]} AND ("Longitude" = 0 OR "Longitude" = 0 OR "Longitude" = 1 OR "Longitude" = 1);'
        print(sql_query)
        stream.writelines(sql_query + '\n')
        pass
    stream.close()
    index = index + 1
    if index * step > len(cities_changes):
        break
    pass


# for city_change in cities_changes:
#     sql_query = f'UPDATE {table_name} SET "UpdatedAt" = current_timestamp, "Longitude"={city_change[changes_columns["longitude"]]}, "Latitude"={city_change[changes_columns["latitude"]]} WHERE "Id"={city_change[changes_columns["city_id"]]};'
#     print(sql_query)
#     stream.writelines(sql_query + '\n')

stream.close()
connection.close()
