from time import sleep
from Locating.api_dadata import *
from Locating.api_2gis import *
from city_columns import *
from partners import *
from Tools.utc_offset import *
from bcolors import *
import sqlite3


def has_changes_in_table(id):
    cursor.execute(f"""SELECT count(*) as count
                    FROM changes
                    WHERE "city_id"={id}""")
    return cursor.fetchone()[0] > 0


def count_changes_rows():
    cursor.execute(f"""SELECT count(*) as count
                    FROM changes""")
    return cursor.fetchone()[0]


def add_changes_in_db(row_id, city_name, find_city_name, lon, lat):
    cursor.execute(f"""INSERT INTO changes (city_id, city_name, find_city_name, longitude, latitude)
                  VALUES( {row_id}, '{city_name}', '{find_city_name}', {lon}, {lat});""")
    connection.commit()
    sleep(0.3)
    pass


def update_location_with_2gis(location):
    location_name = location[city_columns['NameI']]
    response = request_search_2gis(location_name)
    if response['meta']['code'] == 404 or 'result' not in response:
        print('2gis не нашёл локации по запросу: ' + str(location_name))
        add_changes_in_db(location[city_columns['Id']], location_name, '', -1, -1)
        return
    location_timezone_offset = get_offset_from_timezone(location[city_columns['Timezone']])
    find_locations = response['result']['items']
    find_location_first = find_locations[0]
    find_location_first_name = find_location_first['full_name']
    find_location_first_geo = find_location_first['point']
    find_location_timezone_str = get_timezone_str_from_coordinates(find_location_first_geo["lon"], find_location_first_geo["lat"])
    find_location_timezone_offset = get_offset_from_timezone(find_location_timezone_str)

    response_2gis_search_pharmacies = request_search_pharmacies_2gis(find_location_first_geo["lon"], find_location_first_geo["lat"])
    if 'result' in response_2gis_search_pharmacies:
        has_partners = False
        for pharmacy in response_2gis_search_pharmacies['result']['items']:
            if is_partner_pharmacy(pharmacy['name']):
                has_partners = True
        if has_partners and abs(abs(location_timezone_offset) - abs(find_location_timezone_offset)) <= 1:
            print(f'Часовые пояса отличаются меньше чем на час, обновляем локацию на: {find_location_first_name}')
            add_changes_in_db(location[city_columns['Id']], location_name, find_location_first_name, find_location_first_geo["lon"], find_location_first_geo["lat"])
            return

    while True:
        print_compare_2gis_response_with_location(response, location)
        print('Введи номер локации (или "e" перейти к следующему городу)')
        answer = input()
        if answer == 'e':
            add_changes_in_db(location[city_columns['Id']], location_name, '', -1, -1)
            print('Переходим к следующему городу')
            break
        index = int(answer) - 1
        selected_location = find_locations[index]
        selected_location_geo = selected_location['point']
        lon = selected_location_geo["lon"]
        lat = selected_location_geo["lat"]
        print(f'Выбрали: {selected_location["full_name"]}')
        print_browser_2gis_links(location_name, lon, lat)
        response_2gis_search_pharmacies = request_search_pharmacies_2gis(lon, lat)
        if 'result' in response_2gis_search_pharmacies and 'items' in response_2gis_search_pharmacies['result']:
            has_partners = False
            for pharmacy in response_2gis_search_pharmacies['result']['items']:
                if is_partner_pharmacy(pharmacy['name']):
                    has_partners = True
            if has_partners:
                print('Есть ли партнёры среди найденных аптек: ' + bcolors_true_text)
            else:
                print('Есть ли партнёры среди найденных аптек: ' + bcolors_false_text)
                
        else:
            print('Есть ли партнёры среди найденных аптек: ' + bcolors_false_text)
        print_pharmacies_around_location(response_2gis_search_pharmacies)
        print('Обновляем данные в базе данных (y/n)?')
        answer = input()
        if answer == 'y' or answer == 'Y':
            print(f'Обновляем локацию {location_name}...')
            add_changes_in_db(location[city_columns['Id']], location_name, selected_location["full_name"], lon, lat)
            print(f'Успешно обновили локацию {location_name}.')
            break
        pass


def update_location_with_dadata(location):
    location_name = location[city_columns['NameI']]
    response = request_search_dadata(location_name)
    print_compare_dadata_response_with_location(response, location)
    if response['result'] is None:
        add_changes_in_db(location[city_columns['Id']], location_name, '', -1, -1)
        return
    lon = response["geo_lon"]
    lat = response["geo_lat"]
    find_location_name = response["result"]
    response_timezone_offset = int(response['timezone'][4:])
    location_timezone_offset = get_offset_from_timezone(location[city_columns['Timezone']])
    if response_timezone_offset != location_timezone_offset:
        add_changes_in_db(location[city_columns['Id']], location_name, '', -1, -1)
        print('Не совпали часовые пояса, идём дальше.')
        return
    
    response_2gis_search_pharmacies = request_search_pharmacies_2gis(lon, lat)
    print_pharmacies_around_location(response_2gis_search_pharmacies)
    if response_2gis_search_pharmacies['meta']['code'] == 404 or 'result' not in response_2gis_search_pharmacies or response_2gis_search_pharmacies['result'] is None or len(response_2gis_search_pharmacies['result']['items']) <= 0:
        print("Не найдено аптек поблизости.")
    else:
        for pharmacy in response_2gis_search_pharmacies['result']['items']:
            if is_partner_pharmacy(pharmacy['name']):
                print(f'Рядом с локацией найден партнёр: {pharmacy["name"]}. Обновляем локацию {location_name}...')
                add_changes_in_db(location[city_columns['Id']], location_name, find_location_name, lon, lat)
                print(f'Успешно обновили локацию {location_name}.')
                return

    print_browser_2gis_links(location_name, lon, lat)
    print()
    print('Обновляем данные в базе данных (y/n)?')
    answer = input()
    if answer == 'y' or answer == 'Y':
        print(f'Обновляем локацию {location_name}...')
        add_changes_in_db(location[city_columns['Id']], location_name, find_location_name, lon, lat)
        print(f'Успешно обновили локацию {location_name}.')
    else:
        print(f'Не обновили локацию: {location_name}')
        add_changes_in_db(location[city_columns['Id']], location_name, '', -1, -1)
    pass


def update_location(location):
    update_with_2gis = True
    update_with_dadata = False
    has_changes = has_changes_in_table(location[city_columns["Id"]])
    if has_changes:
        print(f'{location[city_columns["NameI"]]}. Изменения координат уже добавлены в таблицу.')
        return
    print()
    print()
    location_name = location[city_columns['NameI']]
    print(f'Осталось проверить: {str(num_invalid_locations - count_changes_rows())}')
    print(f'Ищем локацию по запросу: {location_name}, Id в бд: {location[city_columns["Id"]]}')
    if update_with_2gis:
        update_location_with_2gis(location)
    if update_with_dadata:
        update_location_with_dadata(location)
    pass


def create_table_if_not_exist():
    cursor.execute("""CREATE TABLE IF NOT EXISTS main.changes (
                  id INTEGER PRIMARY KEY,
                  city_id int not null,
                  city_name text not null,
                  find_city_name text not null,
                  longitude DOUBLE PRECISION not null,
                  latitude DOUBLE PRECISION not null,
                  constraint city_id_constrant unique (city_id)
                  );""")
    connection.commit()


connection = sqlite3.connect("Data/cities_locations_changes.db")
cursor = connection.cursor()
create_table_if_not_exist()

cursor.execute(f"""SELECT x.* 
                    FROM locations_agglomeration_city x 
                    WHERE ("Longitude" = 0
                          or "Latitude" = 0
                          or "Longitude" = 1
                          or "Latitude" = 1)
                          -- and "Deleted" = false
                    ORDER BY "Id" ASC""")
invalid_locations = cursor.fetchall()
num_invalid_locations = len(invalid_locations)


print('Найдено локаций с невалидными координатами: ' + str(len(invalid_locations)))
print('Осталось заполнить: ' + str(num_invalid_locations - count_changes_rows()))
print()
for invalid_location in invalid_locations:
    update_location(invalid_location)

connection.close()
